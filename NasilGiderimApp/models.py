from django.db import models
from datetime import datetime

# Create your models here.
class Hat(models.Model):
	isim = models.CharField(max_length=5)

class Durak(models.Model):
	isim = models.CharField(max_length=50)

class Iliski(models.Model):
	hatID = models.ForeignKey(Hat)
	durakID = models.ForeignKey(Durak)

class Saat(models.Model):
	hatID = models.ForeignKey(Hat)
	saat = models.TimeField()